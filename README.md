# Geolocator Importer

Esse projeto é um utilitário desenvolvido para ser utilizado em conjunto com projeto [Geolocator](https://bitbucket.org/edearaujo/geolocator/src/master/). Ele deve ser utilizado para fazer a higienização, transformação e importação da massa de dados para um banco de dados espacial.

### 1. Como utilizar?

- Requisitos: [NodeJS](https://nodejs.org/), [Docker](https://www.docker.com).

1) Primeiro, siga as instruções no repositória da aplicação [Geolocator](https://bitbucket.org/edearaujo/geolocator/src/master/) para fazer um build e subir a aplicação localmente. Essa etapa é necessária pois é a aplicação Geolocator que monta as tabelas no banco de dados.

2) Faça um clone deste repositório e navegue até a pasta. Lá execute:

```
npm install
node index
```

Pronto!