const parserService = require('./services/parserService');
const uuidGenerator = require('uuid/v4');
const connection = require('./resources/connection.json');
const Client = require('pg').Client;
const client = new Client(connection);

const queryString = 'insert into store(name, street, number, zip_code, city, state, id, location) values($1, $2, $3, $4, $5, $6, $7, $8)';

const load = async () => {
    const data = await parserService.parse();

    const totalRows = data.length;
    let failedRows = 0;

    console.log('Beginning insertion of', totalRows, 'stores.');
    client.connect();

    try {
        const promises = data.map(store => {
            if(!store.latitude || !store.longitude) {
                failedRows++;
                return;
            }
    
            store.uuid = uuidGenerator();
            store.location = 'SRID=3623;POINT(' + store.latitude + ' ' + store.longitude + ')';
            delete store.latitude;
            delete store.longitude;
    
            return client.query(queryString, Object.values(store))
                   .catch(e => {
                        console.error('Insertion failed:', e);
                        console.error('On row:', store);
                        failedRows++;
                   });
        });
    
        await Promise.all(promises);
        console.log('The insertion process has completed successfully.');
        console.log('Inserted:', totalRows - failedRows);
        console.log('Failed:', failedRows);
    } catch(e) {
        console.error('Fatal error:', e)
    } finally {
        client.end();
    }
};

load();

module.exports = {
    load
};