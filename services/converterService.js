const proj4 = require('proj4');

proj4.defs([
    [
        'EPSG:3623',
        '+proj=tmerc +lat_0=40 +lon_0=-76.58333333333333 +k=0.9999375 +x_0=250000 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
    ]
]);

const convert = (point) => {
    if(!point.latitude || !point.longitude) {
        throw 'invalid arguments';
    }
    
    const projection = proj4('EPSG:4326', 'EPSG:3623', [point.latitude, point.longitude]);
    return {
        latitude: projection[0],
        longitude: projection[1]
    };
};

module.exports = {
    convert
};