const csv = require('csv-parser');
const fs = require('fs');
const converterService = require('./converterService');
const regexLon = /^\{\'longitude\': \'[+-]*\d+\.\d+\'/;
const regexLat = /, \'latitude\': \'[+-]*\d+.\d+\'}$/;
    

const parse = () => {
    const results = [];
    let totalRows = 0;
    let failedRows = 0;

    console.log('Beginning parsing of csv file.');

    return new Promise((res, rej) => {
        fs.createReadStream('./stores.csv')
        .pipe(csv())
        .on('data', data => {
            totalRows++;

            if(!regexLon.test(data["Location"]) || !regexLat.test(data["Location"])) {
                failedRows++;
                return;
            }

            let textResult = regexLon.exec(data["Location"])[0];
            textResult += regexLat.exec(data["Location"])[0];
            
            try {
                textResult = textResult.replace(/\'/g, '"');

                let result = JSON.parse(textResult);
                result.latitude = Number.parseFloat(result.latitude);
                result.longitude = Number.parseFloat(result.longitude);
                result = converterService.convert(result);

                result.name = data["Entity Name"].trim();
                result.street = data["Street Name"].trim();
                result.number = data["Street Number"].trim();
                result.zipCode = data["Zip Code"].trim();
                result.city = data["City"].trim();
                result.state = data["State"].trim();

                results.push(result);
            } catch(e) {
                failedRows++;
                return;
            }
            
        })
        .on('end', () => {
            console.log('The parsing process has completed successfuly.');
            console.log('Read:', totalRows - failedRows);
            console.log('Failed:', failedRows);
            res(results);
        });
    });
};

module.exports = {
    parse
};